-- Create tables

CREATE TABLE Users (
    Id SERIAL PRIMARY KEY,
    AwsId UUID NOT NULL,
    Email VARCHAR(100) NOT NULL,
    Name VARCHAR(100) NOT NULL
);

CREATE INDEX Users_AwsId_Index ON public.Users (AwsId);

CREATE TABLE Notes (
    Id SERIAL PRIMARY KEY,
    UserId INT NOT NULL,
    Message VARCHAR(500) NOT NULL,
    Timestamp TIMESTAMP NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Users(Id) ON DELETE CASCADE
);

-- Insert Data

INSERT INTO public.Users(AwsId, Email, Name) 
	VALUES ('c66b4c94-3650-4d57-806b-07ef25508def',
	        'john.doe@gmail.com','John Doe');

INSERT INTO public.Notes(UserId, Message, Timestamp)
    VALUES ((SELECT Id FROM public.Users WHERE Email = 'john.doe@gmail.com'),
	        'This is my first note!',
	        '2024-01-01');

INSERT INTO public.Notes(UserId, Message, Timestamp)
    VALUES ((SELECT Id FROM public.Users WHERE Email = 'john.doe@gmail.com'),
	        '... and my second note!',
	        '2024-01-02');
			
INSERT INTO public.Notes(UserId, Message, Timestamp)
    VALUES ((SELECT Id FROM public.Users WHERE Email = 'john.doe@gmail.com'),
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	        '2024-01-02 08:30:00');
			